package object;

import java.util.ArrayList;

public class ProvinceArray {
	private ArrayList<Province> listProvince = new ArrayList<Province>();

	public ArrayList<Province> getListProvince() {
		return listProvince;
	}

	public void setListProvince(ArrayList<Province> listProvince) {
		this.listProvince = listProvince;
	}

	public Province findProvincebyName(String name) {
		Province prov = new Province();

		for (int i = 0; i < listProvince.size(); i++) {
			if (listProvince.get(i).getM_sProvinceName().equals(name)) {
				prov = listProvince.get(i);
				return prov;
			}
		}
		return null;
	}

	public int findProvinceIDbyName(String name) {
		int provID = -1;

		for (int i = 0; i < listProvince.size(); i++) {
			if (listProvince.get(i).getM_sProvinceName().equals(name)) {
				provID = listProvince.get(i).getM_iProvinceID();
				return provID;
			}
		}
		return provID;
	}
}
