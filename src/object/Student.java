package object;

import java.sql.Date;

public class Student {
	private int m_iStudentID;
	private String m_sStudentName;
	private int m_iProvinceID;
	private String m_Birthday;
	private int m_isMale;
	
	public int getM_isMale() {
		return m_isMale;
	}
	public void setM_isMale(int m_isMale) {
		this.m_isMale = m_isMale;
	}

	private float m_fMathScore;
	private float m_fPhysicsScore;
	private float m_fCheScore;

	public Student(){
		
	}
	public Student(int id, String name, int provinceId, String date, int isMale, float math, float physics, float che){
		this.m_iStudentID =id;
		this.m_sStudentName= name;
		this.m_iProvinceID = provinceId;
		this.m_Birthday = date;
		this.m_isMale = isMale;
		this.m_fMathScore = math;
		this.m_fPhysicsScore = physics;
		this.m_fCheScore =che;
	}
	public int getM_iStudentID() {
		return m_iStudentID;
	}

	public void setM_iStudentID(int m_iExamineeID) {
		this.m_iStudentID = m_iExamineeID;
	}

	public String getM_sStudentName() {
		return m_sStudentName;
	}

	public void setM_sStudentName(String m_sExamineeName) {
		this.m_sStudentName = m_sExamineeName;
	}

	public int getM_iProvinceID() {
		return m_iProvinceID;
	}

	public void setM_iProvinceID(int m_iProvinceID) {
		this.m_iProvinceID = m_iProvinceID;
	}

	public String getM_Birthday() {
		return m_Birthday;
	}

	public void setM_Birthday(String m_Birthday) {
		this.m_Birthday = m_Birthday;
	}

	public float getM_fMathScore() {
		return m_fMathScore;
	}

	public void setM_fMathScore(float m_fMathScore) {
		this.m_fMathScore = m_fMathScore;
	}

	public float getM_fPhysicsScore() {
		return m_fPhysicsScore;
	}

	public void setM_fPhysicsScore(float m_fPhysicsScore) {
		this.m_fPhysicsScore = m_fPhysicsScore;
	}

	public float getM_fCheScore() {
		return m_fCheScore;
	}

	public void setM_fCheScore(float m_fCheScore) {
		this.m_fCheScore = m_fCheScore;
	}
}
