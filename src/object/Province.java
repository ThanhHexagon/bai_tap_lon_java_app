package object;

public class Province {
	private int m_iProvinceID;
	private String m_sProvinceName;

	public Province() {

	}

	public Province(int id, String name) {
		this.m_iProvinceID = id;
		this.m_sProvinceName = name;
	}

	public int getM_iProvinceID() {
		return m_iProvinceID;
	}

	public void setM_iProvinceID(int m_iProvinceID) {
		this.m_iProvinceID = m_iProvinceID;
	}

	public String getM_sProvinceName() {
		return m_sProvinceName;
	}

	public void setM_sProvinceName(String m_sProvinceName) {
		this.m_sProvinceName = m_sProvinceName;
	}

}
