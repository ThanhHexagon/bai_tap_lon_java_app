package userinterface;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import manager.InputManager;
import manager.OutputManager;

public class MenuPanel extends JPanel implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	 * Attributes
	 */
	File file;
	JFileChooser fileChooser; 
	JMenuBar menuBar;
	JMenu fileMenu, aboutMenu;
	JMenuItem openItem, closeItem, aboutItem;
	
	InforPanel panel = new InforPanel();
	/*
	 * Methods
	 */
	
	// Constructor
	public MenuPanel(){
		fileChooser = new JFileChooser();
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		
		openItem = new JMenuItem("Save");	
		openItem.addActionListener(this);
		fileMenu.add(openItem);
		
		closeItem = new JMenuItem("Close");
		closeItem.addActionListener(this);
		fileMenu.add(closeItem);
		
		menuBar.add(fileMenu);
		
		aboutMenu = new JMenu("About");
		aboutItem = new JMenuItem("About...");
		aboutMenu.add(aboutItem);
		
		menuBar.add(aboutMenu);
		
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.add(menuBar);
		//this.setAlignmentX(LEFT_ALIGNMENT);
		
	}

	public JMenuBar getMenuBar() {
		return menuBar;
	}

	public void setMenuBar(JMenuBar menuBar) {
		this.menuBar = menuBar;
	}

	public JMenu getFileMenu() {
		return fileMenu;
	}

	public void setFileMenu(JMenu fileMenu) {
		this.fileMenu = fileMenu;
	}

	public JMenu getAboutMenu() {
		return aboutMenu;
	}

	public void setAboutMenu(JMenu aboutMenu) {
		this.aboutMenu = aboutMenu;
	}

	public JMenuItem getOpenItem() {
		return openItem;
	}

	public void setOpenItem(JMenuItem openItem) {
		this.openItem = openItem;
	}

	public JMenuItem getCloseItem() {
		return closeItem;
	}

	public void setCloseItem(JMenuItem closeItem) {
		this.closeItem = closeItem;
	}

	public JMenuItem getAboutItem() {
		return aboutItem;
	}

	public void setAboutItem(JMenuItem aboutItem) {
		this.aboutItem = aboutItem;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == openItem) {
			fileChooser.showOpenDialog(this);
			fileChooser.setDialogTitle("Save file...");
			File file = fileChooser.getSelectedFile();
			OutputManager.getInstance().writeStudentFile(file.toPath().toString(), InputManager.getInstance().getListStudent());
		}
		if(e.getSource() == closeItem) {
			System.exit(0);
		}
	}

	
}
