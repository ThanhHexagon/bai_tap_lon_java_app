package userinterface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableModel;

import manager.InputManager;
import model.CustomTableModel;
import object.Province;
import object.Student;

public class InforPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * Attributes
	 */
	// child panels
	JPanel pnlTable;
	JPanel pnlInfo;
	// Label
	private JLabel lblID;
	private JLabel lblName;
	private JLabel lblBirthday;
	private JLabel lblBirthplace;
	private JLabel lblMath;
	private JLabel lblPhysics;
	private JLabel lblChe;
	private JLabel lblTotal;
	private JLabel lblSex;
	// Textbox
	private JTextField txtID;
	private JTextField txtName;
	private JTextField txtBirthday;
	private JTextField txtBirthplace;
	private JTextField txtMath;
	private JTextField txtPhysics;
	private JTextField txtChe;
	private JTextField txtTotal;
	// Radio button
	private JRadioButton rbtnMale;
	private JRadioButton rbtnFemale;
	private ButtonGroup grBtnSex;
	// Panel
	private JPanel pnlSex;

	// Logic attributes
	boolean isChoosed = false;
	//

	ArrayList<Student> listStudent;
	ArrayList<Province> listProvince ;
	// InputManager.getInstance().getListProvince();
	Student currentSelectedStudent;
	Province currentSelectedProvince;
	TableModel tableModel;
	JTable tblStudent;
	JScrollPane pnScroll;

	/*
	 * Methods
	 */
	// Constructor
	public InforPanel() {
		listStudent = InputManager.getInstance().getListStudent();
		listProvince = InputManager.getInstance().getListProvince();
		
		tableModel = new CustomTableModel();
		tblStudent = new JTable(tableModel);
		
		//
		pnlTable = new JPanel();
		pnlInfo = new JPanel();
		
		//
		lblID = new JLabel("ID");
		lblName = new JLabel("Name");
		lblBirthday = new JLabel("Birthday");
		lblBirthplace = new JLabel("Birthplace");
		lblMath = new JLabel("Math score");
		lblPhysics = new JLabel("Physics score");
		lblChe = new JLabel("Chemistry score");
		lblTotal = new JLabel("Total");
		lblSex = new JLabel("Sex");
		//
		txtID = new JTextField();
		txtID.setEnabled(false);
		//
		txtName = new JTextField();
		txtName.setToolTipText("Input your name here");
		txtName.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		//
		txtBirthday = new JTextField();
		txtBirthday.setToolTipText("Input your birthday here");
		txtBirthday.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		//
		txtBirthplace = new JTextField();
		txtBirthplace.setToolTipText("Input your birthplace here");
		txtBirthplace.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		//
		txtMath = new JTextField();
		txtMath.setToolTipText("Input your math score here");
		txtMath.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		//
		txtPhysics = new JTextField();
		txtPhysics.setToolTipText("Input your physics score here");
		txtPhysics.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		//
		txtChe = new JTextField();
		txtChe.setToolTipText("Input your chemistry score here");
		txtChe.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		//
		txtTotal = new JTextField();
		txtTotal.setEnabled(false);
		//
		rbtnFemale = new JRadioButton("Female");
		rbtnMale = new JRadioButton("Male");
		//
		pnlSex = new JPanel();
		// put 2 radio button into a group button
		grBtnSex = new ButtonGroup();
		grBtnSex.add(rbtnMale);
		grBtnSex.add(rbtnFemale);
		pnlSex.setLayout(new FlowLayout());
		pnlSex.add(rbtnMale);
		pnlSex.add(rbtnFemale);
		// table
		tblStudent.setPreferredScrollableViewportSize(tblStudent.getPreferredSize());
		tblStudent.setFillsViewportHeight(true);
		tblStudent.getTableHeader().setReorderingAllowed(false);

		pnScroll = new JScrollPane(tblStudent);
		// add table to child panel
		pnlTable.setLayout(new BorderLayout());
		pnlTable.add(pnScroll, BorderLayout.CENTER);
		//

		// Set border
		TitledBorder title;
		Border blackline = BorderFactory.createLineBorder(Color.black);
		title = BorderFactory.createTitledBorder(blackline, "Student information");
		pnlInfo.setBorder(title);
		// Set layout
		pnlInfo.setLayout(new GridLayout(5, 4));
		// row 1
		pnlInfo.add(lblID);
		pnlInfo.add(txtID);
		pnlInfo.add(lblMath);
		pnlInfo.add(txtMath);
		// row 2
		pnlInfo.add(lblName);
		pnlInfo.add(txtName);
		pnlInfo.add(lblPhysics);
		pnlInfo.add(txtPhysics);
		// row 3
		pnlInfo.add(lblBirthplace);
		pnlInfo.add(txtBirthplace);
		pnlInfo.add(lblChe);
		pnlInfo.add(txtChe);
		// row 4
		pnlInfo.add(lblBirthday);
		pnlInfo.add(txtBirthday);
		pnlInfo.add(lblTotal);
		pnlInfo.add(txtTotal);
		// row 5
		pnlInfo.add(lblSex);
		pnlInfo.add(pnlSex);
		// this.add(rbtnMale);
		// this.add(rbtnFemale);

		// add 2 child panel to parent panel
		this.setLayout(new BorderLayout());
		this.add(pnlTable, BorderLayout.CENTER);
		this.add(pnlInfo, BorderLayout.SOUTH);
		setEnable(false);

		//
		tblStudent.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				int id;
				String name;
				String provName;
				int provId;
				String date;
				String sex;
				int sexId;
				String strMath;
				String strPhysics;
				String strChe;

				float math;
				float physics;
				float che;
				float total;

				int index = tblStudent.getSelectedRow();
				CustomTableModel model = (CustomTableModel) tblStudent.getModel();
				txtID.setText(model.getValueAt(index, 1).toString());
				txtName.setText(model.getValueAt(index, 2).toString());
				txtBirthplace.setText(model.getValueAt(index, 3).toString());
				txtBirthday.setText(model.getValueAt(index, 4).toString());
				sex = model.getValueAt(index, 5).toString();
				if (sex.equals("Male")) {
					rbtnMale.setSelected(true);
				} else {
					rbtnFemale.setSelected(true);
				}
				math = (float) model.getValueAt(index, 6);
				txtMath.setText(model.getValueAt(index, 6).toString());
				physics = (float) model.getValueAt(index, 7);
				txtPhysics.setText(model.getValueAt(index, 7).toString());
				che = (float) model.getValueAt(index, 8);
				txtChe.setText(model.getValueAt(index, 8).toString());
				total = che + math + physics;
				txtTotal.setText(Float.toString(total));
				isChoosed = true;
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	public void setEnable(boolean a) {
		txtName.setEnabled(a);
		txtBirthday.setEnabled(a);
		txtBirthplace.setEnabled(a);
		txtMath.setEnabled(a);
		txtPhysics.setEnabled(a);
		txtChe.setEnabled(a);

	}

	public void clearField() {

		txtName.setText("");
		txtBirthday.setText("");
		txtBirthplace.setText("");
		txtMath.setText("");
		txtPhysics.setText("");
		txtChe.setText("");
		txtTotal.setText("");
		rbtnFemale.setSelected(false);
		;
		rbtnMale.setSelected(false);
	}

	public int getLastId() {
		return InputManager.getInstance().getListStudent().get(InputManager.getInstance().getListStudent().size() - 1)
				.getM_iStudentID(); // minus 1 because array starts from 0, not 1
	}

	public JTextField getTxtID() {
		return txtID;
	}

	public void setTxtID(JTextField txtID) {
		this.txtID = txtID;
	}

	public JTextField getTxtName() {
		return txtName;
	}

	public void setTxtName(JTextField txtName) {
		this.txtName = txtName;
	}

	public JTextField getTxtBirthday() {
		return txtBirthday;
	}

	public void setTxtBirthday(JTextField txtBirthday) {
		this.txtBirthday = txtBirthday;
	}

	public JTextField getTxtBirthplace() {
		return txtBirthplace;
	}

	public void setTxtBirthplace(JTextField txtBirthplace) {
		this.txtBirthplace = txtBirthplace;
	}

	public JTextField getTxtMath() {
		return txtMath;
	}

	public void setTxtMath(JTextField txtMath) {
		this.txtMath = txtMath;
	}

	public JTextField getTxtPhysics() {
		return txtPhysics;
	}

	public void setTxtPhysics(JTextField txtPhysics) {
		this.txtPhysics = txtPhysics;
	}

	public JTextField getTxtChe() {
		return txtChe;
	}

	public void setTxtChe(JTextField txtChe) {
		this.txtChe = txtChe;
	}

	public JTextField getTxtTotal() {
		return txtTotal;
	}

	public void setTxtTotal(JTextField txtTotal) {
		this.txtTotal = txtTotal;
	}

	public JRadioButton getRbtnMale() {
		return rbtnMale;
	}

	public void setRbtnMale(JRadioButton rbtnMale) {
		this.rbtnMale = rbtnMale;
	}

	public JRadioButton getRbtnFemale() {
		return rbtnFemale;
	}

	public void setRbtnFemale(JRadioButton rbtnFemale) {
		this.rbtnFemale = rbtnFemale;
	}

	public TableModel getTableModel() {
		return tableModel;
	}

	public void setTableModel(TableModel tableModel) {
		this.tableModel = tableModel;
	}

	public boolean isChoosed() {
		return isChoosed;
	}

	public void setChoosed(boolean isChoosed) {
		this.isChoosed = isChoosed;
	}

	public JTable getTblStudent() {
		return tblStudent;
	}

	public void setTblStudent(JTable tblStudent) {
		this.tblStudent = tblStudent;
	}

	public ArrayList<Student> getListStudent() {
		return listStudent;
	}

	public void setListStudent(ArrayList<Student> listStudent) {
		this.listStudent = listStudent;
	}

	public ArrayList<Province> getListProvince() {
		return listProvince;
	}

	public void setListProvince(ArrayList<Province> listProvince) {
		this.listProvince = listProvince;
	}

	public int findProvinceByName(String name) {
		for (int i = 0; i < InputManager.getInstance().getListProvince().size(); i++) {
			if (InputManager.getInstance().getListProvince().get(i).getM_sProvinceName().equals(name)) {
				return InputManager.getInstance().getListProvince().get(i).getM_iProvinceID();
			}
		}
		return -1;
	}
}
