package userinterface;

import javax.swing.*;
import javax.swing.border.Border;

import manager.InputManager;
import manager.OutputManager;
import model.CustomTableModel;
import object.Student;
import object.Province;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

public class MainFrame extends JFrame implements ActionListener {
	/*
	 * Noname attributes
	 */
	
	/*
	 * panel attributes
	 */
	InforPanel pnlInfor;
	JPanel pnlCenter;
	FilterPanel pnlFilter;
	JPanel pnlMain;
	FunctionPanel pnlFunction;
	MenuPanel pnlMenu;
	JPanel pnlNorth;
	/*
	 * component attributes
	 */
	JButton btnFilter;
	JButton btnNew;
	JButton btnDelete;
	JButton btnEdit;
	JButton btnApply;
	JButton btnCancel;
	// size of frame
	final int MAIN_FRAME_WIDTH = 1080;
	final int MAIN_FRAME_HEIGHT = 720;

	// constructor
	public MainFrame() {
		// initializing
		pnlFunction = new FunctionPanel();
		pnlCenter = new JPanel();
		pnlMain = new JPanel();
		pnlFilter = new FilterPanel();
		pnlInfor = new InforPanel();
		
		btnNew = pnlFunction.getBtnNew();
		btnNew.addActionListener(this);
		
		btnDelete = pnlFunction.getBtnDelete();
		btnDelete.addActionListener(this);
		
		btnEdit = pnlFunction.getBtnEdit();
		btnEdit.addActionListener(this);
		
		btnApply = pnlFunction.getBtnApply();
		btnApply.addActionListener(this);
		
		btnCancel = pnlFunction.getBtnCancel();
		btnCancel.addActionListener(this);
		
		btnFilter = pnlFilter.getBtnFilter();
		btnFilter.addActionListener(this);
		//
		
		// pnlTable = new TablePanel();
		pnlMenu = new MenuPanel();
		pnlNorth = new JPanel();
		// setup pnlNorth
		pnlNorth.setLayout(new BorderLayout());
		pnlNorth.add(pnlMenu, BorderLayout.NORTH);
		pnlNorth.add(pnlFilter, BorderLayout.CENTER);
		// setup pnlCenter
		pnlCenter.setLayout(new BorderLayout());
		pnlCenter.add(pnlInfor, BorderLayout.CENTER);
		
		// setup pnlMain
		Border blackline = BorderFactory.createLineBorder(Color.black);
		pnlMain.setSize(MAIN_FRAME_WIDTH, MAIN_FRAME_HEIGHT);
		pnlMain.setLayout(new BorderLayout());
		pnlMain.setBorder(blackline);
		//pnlMain.add(pnlNorth, BorderLayout.NORTH);
		pnlMain.add(pnlCenter, BorderLayout.CENTER);
		//pnlMain.add(pnlFunction, BorderLayout.SOUTH);

		this.add(pnlMain);
		this.setSize(MAIN_FRAME_WIDTH, MAIN_FRAME_HEIGHT);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == pnlFunction.getBtnNew()) {
			System.out.println("New Pressed");
			pnlInfor.setEnable(true);
			int a = pnlInfor.getLastId();
			pnlInfor.getTxtID().setText(Integer.toString(a+1));
			pnlInfor.clearField();
		}
		
		if (e.getSource() == pnlFunction.getBtnDelete()) {
			System.out.println("Delete Pressed");
			if(pnlInfor.isChoosed() == false) {
				
			}else {
				
				int selectedRow = pnlInfor.getTblStudent().getSelectedRow();
				pnlInfor.getListStudent().remove(selectedRow);				
				CustomTableModel model = (CustomTableModel) pnlInfor.getTblStudent().getModel();
				model.fireTableRowsDeleted(selectedRow, selectedRow);
				model.fireTableDataChanged();
				
			}
		}
		if (e.getSource() == pnlFunction.getBtnEdit()) {
			System.out.println("Edit Pressed");
			pnlInfor.setEnable(true);
			
		}
		if (e.getSource() == pnlFunction.getBtnApply()) {
			
			System.out.println("Apply Pressed");
			//System.out.println(pnlInfor.getTxtID().getText().toString().trim());
			int id = Integer.parseInt(pnlInfor.getTxtID().getText().trim());
			String name = pnlInfor.getTxtName().getText();
			String province = pnlInfor.getTxtBirthplace().getText().trim();
			int provinceId = pnlInfor.findProvinceByName(province);
			String date = pnlInfor.getTxtBirthday().getText().trim();
			int isMale = 0;
			if(pnlInfor.getRbtnMale().isSelected() == true) {
				isMale =1;
			}
			
			float math = Float.parseFloat(pnlInfor.getTxtMath().getText().trim());
			float physics = Float.parseFloat(pnlInfor.getTxtPhysics().getText().trim());
			float che = Float.parseFloat(pnlInfor.getTxtChe().getText().trim());
			Student newStu = new Student(id, name, provinceId, date, isMale, math, physics,che);
			
			pnlInfor.getListStudent().add(newStu);
			
			CustomTableModel model = (CustomTableModel) pnlInfor.getTblStudent().getModel();
			model.fireTableDataChanged();
			//outputManager.writeStudentFile("E:/Java-workspace/BaiTapLon/resources/output.txt",pnlInfor.getListStudent());
			
		}
		if (e.getSource() == pnlFunction.getBtnCancel()) {
			System.out.println("Cancel Pressed");
			pnlInfor.clearField();
			pnlInfor.setEnable(false);
		}
		
		if(e.getSource()== pnlFilter.getBtnFilter()) {
			System.out.println("pressed");
			ArrayList<Student> list = new ArrayList<Student>();
			System.out.println(pnlFilter.getTxtProvince().getText().trim());
			if( pnlFilter.getTxtProvince().getText().trim() != null){	
				int id = InputManager.getInstance().findProvinceIDbyName(pnlFilter.getTxtProvince().getText().trim());
				for(int i = 0; i< InputManager.getInstance().getListStudent().size(); i++) {		
					if(InputManager.getInstance().getListStudent().get(i).getM_iProvinceID() == id) {		
						list.add(InputManager.getInstance().getListStudent().get(i));
					}
				}
			}
			//pnlInfor.setListStudent(list);
			
			CustomTableModel model = (CustomTableModel) pnlInfor.getTblStudent().getModel();
			model.setListStudent(list);
		}
		
	}
}
