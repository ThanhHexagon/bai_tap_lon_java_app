package userinterface;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;

public class FunctionPanel extends JPanel {
	/*
	 * Attributes
	 */
	
	private JButton btnNew;
	private JButton btnDelete;
	private JButton btnEdit;
	private JButton btnApply;
	private JButton btnCancel;
	
	/*
	 * Methods
	 */
	// Constructor
	@SuppressWarnings("deprecation")
	public FunctionPanel() {
		// initializing buttons
		btnNew = new JButton("New");
		btnApply = new JButton("Apply");
		btnEdit = new JButton("Edit");
		btnDelete = new JButton("Delete");	
		btnCancel = new JButton("Cancel");
		//
		btnEdit.enable(false);
		btnDelete.enable(false);
		btnCancel.enable(false);
		btnApply.enable(false);
		
		//
		this.setLayout(new FlowLayout());
		this.add(btnNew);
		this.add(btnDelete);
		this.add(btnEdit);
		this.add(btnApply);
		this.add(btnCancel);
		//
		//btnNew.addActionListener(this);
	}

	public JButton getBtnNew() {
		return btnNew;
	}

	public void setBtnNew(JButton btnNew) {
		this.btnNew = btnNew;
	}

	public JButton getBtnDelete() {
		return btnDelete;
	}

	public void setBtnDelete(JButton btnDelete) {
		this.btnDelete = btnDelete;
	}

	public JButton getBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(JButton btnEdit) {
		this.btnEdit = btnEdit;
	}

	public JButton getBtnApply() {
		return btnApply;
	}

	public void setBtnApply(JButton btnApply) {
		this.btnApply = btnApply;
	}

	public JButton getBtnCancel() {
		return btnCancel;
	}

	public void setBtnCancel(JButton btnCancel) {
		this.btnCancel = btnCancel;
	}

	

	

}
