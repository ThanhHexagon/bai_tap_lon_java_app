package userinterface;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import manager.InputManager;
import object.Student;

public class FilterPanel extends JPanel {
	// constant
	final int TXT_BIRTHDAY_WIDTH = 100;
	final int TXT_BIRTHDAY_HEIGHT = 30;
	final int TXT_PROVINCE_WIDTH = 100;
	final int TXT_PROVINCE_HEIGHT = 30;
	final int BTN_FILTER_HEIGHT = 50;
	final int BTN_FILTER_WIDTH = 100;
	final int PNL_FILTER_HEIGHT = 300;
	final int PNL_FILTER_WIDTH = 1070;
	// attributes
	private JButton btnFilter;
	private JTextField txtBirthday;
	private JTextField txtProvince;
	private JLabel lblBirthDay;
	private JLabel lblProvince;

	public FilterPanel() {
		// preset

		lblBirthDay = new JLabel("Birthday");
		lblProvince = new JLabel("Province");

		txtBirthday = new JTextField();
		txtBirthday.setToolTipText("Input your birthday here");
		txtBirthday.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		txtBirthday.setSize(TXT_BIRTHDAY_WIDTH, TXT_BIRTHDAY_HEIGHT);

		txtProvince = new JTextField();
		txtProvince.setToolTipText("Input name of your province here");
		txtProvince.setCursor(new Cursor(Cursor.TEXT_CURSOR));
		txtProvince.setSize(TXT_PROVINCE_WIDTH, TXT_PROVINCE_HEIGHT);

		btnFilter = new JButton();
		btnFilter.setText("Filter");
		btnFilter.setSize(BTN_FILTER_WIDTH, BTN_FILTER_HEIGHT);

		TitledBorder title;
		Border blackline = BorderFactory.createLineBorder(Color.black);
		title = BorderFactory.createTitledBorder(blackline, "Filter");
		this.setBorder(title);

		// set layout of panel and add component

		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.CENTER;
		gbc.ipady = 0;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 0.5;
		gbc.gridwidth = 1;
		this.add(lblBirthDay, gbc);

		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.ipady = 15;
		gbc.ipadx = 100;
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.gridwidth = 3;
		this.add(txtBirthday, gbc);

		gbc.fill = GridBagConstraints.CENTER;
		gbc.gridx = 4;
		gbc.ipadx = 0;
		gbc.ipady = 0;
		gbc.gridy = 0;
		gbc.weightx = 0.5;
		gbc.gridwidth = 1;
		this.add(lblProvince, gbc);

		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.ipady = 15;
		gbc.ipadx = 100;
		gbc.gridx = 5;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.gridwidth = 3;
		this.add(txtProvince, gbc);

		gbc.fill = GridBagConstraints.CENTER;
		gbc.ipady = 10;
		gbc.ipadx = 100;
		gbc.insets = new Insets(0, 0, 4, 0);
		gbc.gridx = 8;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		this.add(btnFilter, gbc);

		this.setSize(PNL_FILTER_WIDTH, PNL_FILTER_HEIGHT);
	}

	public JButton getBtnFilter() {
		return btnFilter;
	}

	public void setBtnFilter(JButton btnFilter) {
		this.btnFilter = btnFilter;
	}

	public JTextField getTxtBirthday() {
		return txtBirthday;
	}

	public void setTxtBirthday(JTextField txtBirthday) {
		this.txtBirthday = txtBirthday;
	}

	public JTextField getTxtProvince() {
		return txtProvince;
	}

	public void setTxtProvince(JTextField txtProvince) {
		this.txtProvince = txtProvince;
	}
}
