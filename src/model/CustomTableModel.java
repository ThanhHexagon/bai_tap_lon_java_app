package model;

import java.util.ArrayList;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import manager.InputManager;
import object.Province;
import object.Student;

public class CustomTableModel extends AbstractTableModel {

	/*
	 * Attributes
	 */
	
	
	ArrayList<Student> listStudent;
	ArrayList<Province> listProvince;
	// ObjectManager objManager = ObjectManager.getInstance();
	String columnName[] = { "No", "ID", "Name", "Place", "Birthday", "Sex", "Math", "Physics", "Chemistry" };

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;

	public CustomTableModel() {
		
		this.listStudent = InputManager.getInstance().getListStudent();
		this.listProvince = InputManager.getInstance().getListProvince();
		
	}
//	public void addRow(Object[] row) {
//		this.addRow(row);
//	}
	
	public void setListStudent(ArrayList<Student> list) {
		this.listStudent = list;
		this.fireTableDataChanged();
	}
	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return columnName[column];
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnName.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return listStudent.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		// TODO Auto-generated method stub
		Student stu = listStudent.get(row);

		switch (col) {
		case 0: {
			return row + 1;
		}

		case 1:
			return stu.getM_iStudentID();
		case 2:
			return stu.getM_sStudentName();
		case 3: {
			int id = stu.getM_iProvinceID();
			Province prov = null;
			for (int i = 0; i < listProvince.size(); i++) {
				if (id == listProvince.get(i).getM_iProvinceID()) {
					prov = listProvince.get(i);
				}
			}

			return prov.getM_sProvinceName();
		}
		case 4:
			return stu.getM_Birthday();
		case 5: {
			if (stu.getM_isMale() == 1) {
				return "Male";
			} else if (stu.getM_isMale() == 0) {
				return "Female";
			}

		}
		case 6:
			return stu.getM_fMathScore();
		case 7:
			return stu.getM_fPhysicsScore();
		case 8:
			return stu.getM_fCheScore();

		default:
			return null;
		}
		// return null;
	}

}
