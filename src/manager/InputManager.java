package manager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import object.Province;
import object.Student;
/*
 * Singleton pattern
 */
public class InputManager {
	private static InputManager instance;
	private InputManager() {}
	
	public static InputManager getInstance() {
		if(instance == null) {
			instance = new InputManager();
		}
		return instance;
	}
	
	
	/*
	 * Attributes
	 */
	private ArrayList<Student> listStudent = new ArrayList<Student>();
	private ArrayList<Province> listProvince = new ArrayList<Province>();
	
	/*
	 * Methods
	 */

	// read student information
	public void readStudentFile(String file) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));

			String line;

			try {
				while ((line = reader.readLine()) != null) {
					String[] data = line.split(", ");
					int id = Integer.parseInt(data[0]);
					String name = data[1];
					int provinceID = Integer.parseInt(data[2]);
					String date = data[3];
					int isMale = Integer.parseInt(data[4]);
					float math = Float.parseFloat(data[5]);
					float physics = Float.parseFloat(data[5]);
					float chemistry = Float.parseFloat(data[5]);
					// System.out.println(line);
					Student stu = new Student(id, name, provinceID, date, isMale, math, physics, chemistry);
					listStudent.add(stu);
				}
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//return listStudent;
	}

	// read province information
	public void readProvinceFile(String file) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));

			String line;

			try {
				while ((line = reader.readLine()) != null) {
					String[] data = line.split(", ");
					int id = Integer.parseInt(data[0]);
					String name = data[1];
					Province province = new Province(id, name);
					listProvince.add(province);
				}

				reader.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//return listProvince;
	}

	public ArrayList<Student> getListStudent() {
		return listStudent;
	}

	public void setListStudent(ArrayList<Student> listStudent) {
		this.listStudent = listStudent;
	}

	public ArrayList<Province> getListProvince() {
		return listProvince;
	}

	public void setListProvince(ArrayList<Province> listProvince) {
		this.listProvince = listProvince;
	}

	public Province findProvincebyName(String name) {
		Province prov = new Province();

		for (int i = 0; i < listProvince.size(); i++) {
			if (listProvince.get(i).getM_sProvinceName().equals(name)) {
				prov = listProvince.get(i);
				return prov;
			}
		}
		return null;
	}
	
	public int findProvinceIDbyName(String name) {
		int provID = -1;

		for (int i = 0; i < listProvince.size(); i++) {
			if (listProvince.get(i).getM_sProvinceName().equals(name)) {
				provID = listProvince.get(i).getM_iProvinceID();
				return provID;
			}
		}
		return provID;
	}
	

}
