package manager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import object.Student;

public class OutputManager {
	private static OutputManager instance;
	private OutputManager() {}
	
	public static OutputManager getInstance() {
		if(instance == null) {
			instance = new OutputManager();
		}
		return instance;
	}
	public void writeStudentFile(String file, ArrayList<Student> list) {
		try {
			BufferedWriter writer;
			try {
				writer = new BufferedWriter(new FileWriter(file));
				for(int i = 0; i<list.size(); i++) {				
					writer.write(list.get(i).getM_iStudentID() +", " 
							+ list.get(i).getM_sStudentName() + ", "
							+ list.get(i).getM_iProvinceID() + ", "
							+ list.get(i).getM_Birthday() + ", "
							+ list.get(i).getM_isMale()+ ", "
							+ list.get(i).getM_fMathScore()+ ", "
							+ list.get(i).getM_fPhysicsScore()+ ", "
							+ list.get(i).getM_fCheScore());
					writer.newLine();
				}
				writer.flush();
				writer.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
